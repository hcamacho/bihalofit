#!/usr/bin/env python
from distutils.core import setup, Extension
from setuptools.command.build_py import build_py as _build_py


class build_py(_build_py):
    def run(self):
        self.run_command("build_ext")
        return super().run()


bihalofit_module = Extension(
    "_bihalofit", ["bihalofit.i", "bihalofit.cpp"], swig_opts=["-c++"]
)

setup(
    cmdclass={"build_py": build_py},
    name="bihalofit",
    version="0.1",
    author="",
    description="""swig-based bihalofit""",
    ext_modules=[bihalofit_module],
    py_modules=["bihalofit"],
)
