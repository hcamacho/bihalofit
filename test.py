from bihalofit import bihalofit

if __name__ == "__main__":
    # create bihalofit object
    bh_obj = bihalofit()
    # bh_obj = bihalofit(0.047, 0.286, 0.7, 0.82, 0.96, -1)

    # can load desired linear power spectrum P(k) at z=0; if not code will use
    # Eisenstein & Hu (1999) fitting formula for P(k)
    bh_obj.load_pk_data("linear_pk_planck2015.txt")

    z = 0.4
    # redshift
    k1, k2, k3 = 1.0, 1.5, 2.0  # [h/Mpc]

    # calculating D1, r_sigma for a given redshift
    print(bh_obj.calc_D1(z), bh_obj.calc_r_sigma(z))

    # non-linear BS w/o baryons, tree-level BS [(Mpc/h)^6] & baryon ratio for
    # given k1,k2,k3 and z
    print(
        k1,
        k2,
        k3,
        bh_obj.bispec(k1, k2, k3, z),
        bh_obj.bispec_tree(k1, k2, k3, z),
        bh_obj.baryon_ratio(k1, k2, k3, z),
    )
