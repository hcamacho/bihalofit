/********************************************************
 * Swig module description file for wrapping a C++ class.
 * Generate by saying "swig -python -shadow bihalofit.i". 
 * The C module is generated in file bihalofit_wrap.c; here,
 * module 
umber refers to the bihalofit.py shadow class.
 ********************************************************/

%module bihalofit

%{
#include "bihalofit.hpp"
%}

%include bihalofit.hpp
